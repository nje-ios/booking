//
//  Booking.swift
//  Booking
//
//  Created by Test User on 2019. 09. 13..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import Foundation

class Booking {
    var name: String
    var email: String
    var fromDate: Date
    var toDate: Date
    var createdAt: Date
    
    init (_ name: String, _ email: String, _ fromDate: Date, _ toDate: Date) {
        self.name = name
        self.email = email
        self.fromDate = fromDate
        self.toDate = toDate
        
        self.createdAt = Date()
    }
}
