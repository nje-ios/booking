//
//  ViewController.swift
//  Booking
//
//  Created by Test User on 2019. 09. 13..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let unitPrice = 120
    var bookings:[Booking] = []
    
    var diffInDays: Int = 1 {
        didSet {
            print("diffInDays didSet: \(diffInDays)")
            numberOfNightsLabel.text = "Number of nights: \(diffInDays)"
            if diffInDays > 0 {
                totalLabel.text = "Total: $\(unitPrice * diffInDays)"
            }
        }
    }
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var fromDatePicker: UIDatePicker!
    @IBOutlet weak var toDatePicker: UIDatePicker!
    @IBOutlet weak var numberOfNightsLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var numberOfBookingsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        diffInDays = 1
        
        fromDatePicker.addTarget(self, action: #selector(dateChanged(picker:)), for: .valueChanged)
        toDatePicker.addTarget(self, action: #selector(dateChanged(picker:)), for: .valueChanged)
        
        let currentDate = Date()
        toDatePicker.date = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: currentDate)!
    }
    
    
    @IBAction func saveOnTouch(_ sender: UIButton) {
        let name = nameTextField.text!
        let email = emailTextField.text!
        let fromDate = fromDatePicker.date
        let toDate = toDatePicker.date
        
        let booking = Booking(name, email, fromDate, toDate)
        bookings.append(booking)
        
        numberOfBookingsLabel.text = "Number of bookings: \(bookings.count)"
        
        showAlert("Booking saved", getBookingSummary(booking))
        resetView()
    }
    
    func showAlert(_ title: String, _ message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done",
                                      style: UIAlertAction.Style.default,
                                      handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func resetView() {
        nameTextField.text = ""
        emailTextField.text = ""
        numberOfNightsLabel.text = "Number of nights: 1"
        totalLabel.text = "Total: $\(unitPrice)"
        
        let currentDate = Date()
        fromDatePicker.date = currentDate
        toDatePicker.date = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: currentDate)!
    }
    
    func getBookingSummary(_ booking:Booking) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var fee = unitPrice
        let days = getDiffInDays(fromDatePicker.date, fromDatePicker.date)
        if days > 0 {
            fee = unitPrice * days
        }
        
        return "\(booking.name) <\(booking.email)>\n" +
            "\(dateFormatter.string(from: booking.fromDate)) - \(dateFormatter.string(from: booking.toDate))\n" +
        "Total: $\(fee)"
    }
    
    @objc func dateChanged(picker: UIDatePicker) {
        
        diffInDays = getDiffInDays(fromDatePicker.date, toDatePicker.date)
    }
    
    func getDiffInDays(_ from: Date, _ to: Date) -> Int {
        let calendar = Calendar.current
        
        print("from data: \(from), to data: \(to)")
        
        let date1 = calendar.startOfDay(for: from)
        let date2 = calendar.startOfDay(for: to)
        
        let components = calendar.dateComponents([.day],
                                                 from: date1,
                                                 to: date2)
        
        print("difference in days: \(components.day!)")
        
        return components.day!
    }
}
